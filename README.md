![](http://image-store.slidesharecdn.com/cb308e05-7b33-4268-a2c2-67214e5c7c74-large.jpeg)
## Curtains - Source Code Sample
-----------------------------

Curtains: Enhances the functionality of WhatsApp messenger with privacy features.

###The Idea: 
- Look at the app as one giant switchboard. 
- Observe what happens when you turn some switches on, some off.
- Create a feature set based on the results. (Settings pane in Settings.app will allow users to switch on/off

Features included for the following apps:
- WhatsApp.app

### A product of reverse engineering on iOS By: Suhaib Alfageeh
### Language: Objective-C Logos preprocessor directives. 

- 1) Download WhatsApp 
- 2) Dump the binary 
- 3) Analyze assembly 
- 4) Study code-base from dumped binary headers included in WhatsApp binary 
- 5) Inject own logic into hooked classes.
