
/* Copyright (C) alfaDesigns - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Suhaib Alfageeh <suhaib@alfadesigned.com>, 2014-2016
 */

#import "PropertyFile.h"
#import "substrate.h"
BOOL otherRepo = NO;

#define YES __objc_yes

/* ***************************************************
// File:= curtains.xm
//  By: Suhaib Alfaqeeh
//  Copyright 2011-2014, alfaDesigns - All rights reserved.
/****************************************************
WPNoReadReceipts
WPHidePresence
WPHidePlayed

No Read Receipt
No Play Receipt
No Typing Status
No Online Status
 
 /****************************************************
// Feature: WPNoReadReceipts
//  WPNoReadReceipts = [preferences[PREFERENCES_ENABLED_WPNoReadReceipts_KEY] boolValue];
/****************************************************/

/*
=======================
BEGIN METHOD HOOKS
=======================
*/
static NSMutableDictionary *plist = [[NSMutableDictionary alloc] initWithContentsOfFile:@"/var/mobile/Library/Preferences/com.alfadesigns.curtains.plist"];

static BOOL lastSeen = NO;
static BOOL offLine = NO;
static BOOL showDate = NO;
static BOOL showTime = NO;
static BOOL showTimestamp = NO;


static id connection = nil;
%group someTEST
%hook ChatManager

- (BOOL)lastSeenEnabled {


  lastSeen = [[plist objectForKey:@"enabled"]boolValue];  

  if (lastSeen) {
  return NO;
}
else if (!lastSeen) {
  return %orig;
}
}

- (BOOL)lastSeenChangeAllowed {

 lastSeen = [[plist objectForKey:@"enabled"]boolValue];  

if (lastSeen) {
 return YES;
}
else if (!lastSeen) {
  return %orig;
}
}

%end

%hook XMPPConnection

- (BOOL)createPresence:(int)fp8 withNickname:(id)fp12 {

  offLine = [[plist objectForKey:@"enabledit"]boolValue];

 if (offLine) {
   return (FALSE);
}
else if (!offLine) {
  return TRUE;
  }
}
%end

// The hook below is used in WhatsApp 2.10.1 and above :)

%hook WATimestampView

- (BOOL)showDate {

 showDate = [[plist objectForKey:@"enabledDate"]boolValue];

if (showDate) {
 return YES;
}
else if (!showDate) {
  return %orig;
}
}

- (BOOL)showTime {

 showTime = [[plist objectForKey:@"enabledTime"]boolValue];     

 if (showTime) {
 return YES;
}
else if (!showTime) {
   return %orig;
}
}

- (BOOL)showTimestamp {

 showTimestamp = [[plist objectForKey:@"enabledTimestamp"]boolValue];     

if (showTimestamp) {
return YES;
}
else if (!showTimestamp) {
  return %orig;
}
}
%end
%end
%hook ChatViewController
 /****************************************************
// Feature: WPNoReadReceipts
//  WPNoReadReceipts = [preferences[PREFERENCES_ENABLED_WPNoReadReceipts_KEY] boolValue];
/****************************************************/
%group sendReadReceiptsIfNeeded
-(void)sendReadReceiptsIfNeeded { %log; if(Ignition && WPNoReadReceipts){ NSLog(@"Doing Nothing in sendReadReceiptsIfNeeded");}
else{ %orig; }}
%end //END sendReadReceiptsIfNeeded
%end

%hook ChatManager
%group xmppConnection
-(void)xmppConnection:(id)arg1 presenceChanged:(unsigned int)arg2 {
	if(Ignition && WPNoReadReceipts){
		NSLog(@"Doing nothing in xmppConnection presenceChanged");
		return;
	}
	else{
		%orig(arg1, arg2);
	}
}
%end// END Group xmppConnection
%group xmppConnectionJID
-(void)xmppConnection:(id)arg1 presenceChanged:(unsigned int)arg2 forJID:(id)arg3 {
	if(Ignition && WPNoReadReceipts){
		NSLog(@"Doing nothing in xmppConnection presenceChanged");
		return;
	}
	else{
		%orig(arg1, arg2, arg3);
	}
}
%end// END Group xmppConnection


%end //END  ChatManager Class Hook

// Feature: WPHidePresence
%hook XMPPPresenceStanza // =====================================================================================================================XMPPPresenceStanza 
%group nicknameXMPPPresenceStanza // -------------------------------------------------------------------------nicknameXMPPPresenceStanza
-(id)nickname{ %log; if(Ignition && WPHidePresence){ NSLog(@"Doing Nothing in nickname"); return NULL;}
else{ return %orig; }}
%end //End Group nickname
%end //END HOOK nickname

// End Feature Block (experimental)

// Feature: WPHidePresence
%hook XMPPMessageStanza // =====================================================================================================================XMPPMessageStanza 
%group nicknameXMPPMessageStanza // -------------------------------------------------------------------------nicknameXMPPMessageStanza
-(id)nickname{ %log; if(Ignition && WPHidePresence){ NSLog(@"Doing Nothing in nickname"); return NULL;}
else{ return %orig; }}
%end //End Group nickname
%end //END HOOK nickname

// End Feature Block (experimental)

%hook XMPPConnection // =====================================================================================================================XMPPConnection 
%group updatePresence // ------------------------------------------------------------------------------------------updatePresence 
-(void)updatePresence { if(Ignition && WPHidePresence){ return; } else{ %orig; } }
%end

%group sendReadReceiptsForChatMessages// --------------------------------------------------------------------sendReadReceiptsForChatMessages 
-(void)sendReadReceiptsForChatMessages:(id)arg1 { %log; if(Ignition && WPNoReadReceipts){ NSLog(@"Doing Nothing in sendPlayedReceiptForMessage"); return; }
else{ %orig(arg1); }}
%end //END sendReadReceiptsForChatMessages
 /****************************************************
// Feature: WPHidePresence
//  WPHidePresence = [preferences[PREFERENCES_ENABLED_WPHidePresence_KEY] boolValue];
Methods: 
sendPresenceWithNickname
createPresenceWithType
internalSendPresence
setLastPresence
lastPresence
xmppConnection
xmppConnectionJID
/****************************************************/
%group sendPresenceWithNickname // --------------------------------------------------------------------sendPresenceWithNickname 
-(void)sendPresenceWithNickname:(id)arg1 { %log; if(Ignition && WPHidePresence){ NSLog(@"Doing Nothing in sendPresenceWithNickname");  return;}
else{ %orig; }}
%end

%group createPresenceWithType // --------------------------------------------------------------------createPresenceWithType 
-(id)createPresenceWithType:(unsigned int)arg1 nickname:(id)arg2 { %log; if(Ignition && WPHidePresence){ NSLog(@"Doing Nothing in createPresenceWithType"); return NULL; }
else{ %orig(arg1,arg2); }}
%end

%group internalSendPresence // --------------------------------------------------------------------internalSendPresence 
-(void)internalSendPresence:(id)arg1 { %log; id r = arg1;  NSLog(@"xmppConnection  fp8 : %@", r);
 if(Ignition && WPHidePresence){ NSLog(@"Doing Nothing in internalSendPresence"); return; }
else{ %orig(arg1); }}
%end
 /****************************************************
// Feature: WPHidePlayed
//  WPHidePlayed = [preferences[PREFERENCES_ENABLED_WPHidePlayed_KEY] boolValue];
/****************************************************/
%group sendPlayedReceiptForMessage // --------------------------------------------------------------------sendPlayedReceiptForMessage 
-(void)sendPlayedReceiptForMessage:(id)arg1 { %log; if(Ignition && WPHidePlayed){ NSLog(@"Doing Nothing in sendPlayedReceiptForMessage"); return;}
else{ %orig(arg1); }}
%end //END HOOK sendPlayedReceiptForMessage

%end //END Hook XMPPConnection

%hook XMPPClient // =====================================================================================================================XMPPClient 


%group setLastPresence // --------------------------------------------------------------------setLastPresence 
-(void)setLastPresence:(int)arg1 { %log; int r = arg1;  NSLog(@"setLastPresence  fp8 : %@", r);
 if(Ignition && WPHidePresence){ NSLog(@"Doing Nothing in setLastPresence"); return; }
else{ %orig; }}
%end

%group lastPresence // --------------------------------------------------------------------lastPresence 
-(int)lastPresence { %log; if(Ignition && WPHidePresence){ NSLog(@"Returning NIL in lastPresence");  return nil; }
else{ return %orig; }}
%end //END HOOK lastPresence

%group internalSendPlayedReceiptForMessage
-(void)internalSendPlayedReceiptForMessage:(id)arg1 { %log; if(Ignition && WPHidePlayed){ NSLog(@"Doing Nothing in internalSendPlayedReceiptForMessage"); return;}
else{ %orig(arg1); }}
%end

%end //END Hook: XMPPClient
/*
=======================
END OF METHOD HOOKS
=======================
*/
static void PreferencesChangedCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo) { // Action when listener detects a change
    //Perform action when switching a preference specifier
	
    NSLog(@"Killing WhatsApp");
    system("killall WhatsApp"); //Close Skype
    NSLog(@"Killing Settings.app");
    system("killall Settings"); //Close Settings

    
 NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //ceate pointer to peferences path
 
 Ignition = [preferences[PREFERENCES_ENABLED_SKPIgnition_KEY] boolValue];// This switch defines Ignition as the preference boolean
 //Ignition points to the Activate switch in the settings bundle

    WPHidePlayed = [preferences[PREFERENCES_ENABLED_WPHidePlayed_KEY] boolValue];//Sample Switch Defeniton on initialization
    WPHidePresence = [preferences[PREFERENCES_ENABLED_WPHidePresence_KEY] boolValue];//Sample Switch Defeniton on initialization
    WPNoReadReceipts = [preferences[PREFERENCES_ENABLED_WPNoReadReceipts_KEY] boolValue];//Sample Switch Defeniton on initialization

  
}

%ctor { // runs first. This is what happens once the target (net.whatsapp.WhatsApp) is run. Prior to code-insertion. 

        if (![[NSFileManager defaultManager] fileExistsAtPath:@"/var/lib/dpkg/info/org.thebigboss.curtains.list"]){
      otherRepo = YES;
    }
	
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *block) { //add listener

	NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //Define preference path here. (PREFERENCES_PATH).
	
	if (preferences == nil) {
	    preferences = @{ 
		    //PREFERENCES_ENABLED_SKPFeatureOne_KEY : @(NO), Don't forget commmas 
		    PREFERENCES_ENABLED_WPHidePlayed_KEY : @(NO),
		    PREFERENCES_ENABLED_WPHidePresence_KEY : @(NO),
		    PREFERENCES_ENABLED_WPNoReadReceipts_KEY : @(NO),

		    PREFERENCES_ENABLED_SKPIgnition_KEY : @(NO)  // No comma

			
			
			}; // Default preference value to no
	    
	    [preferences writeToFile:PREFERENCES_PATH atomically:YES];
	} else {
 WPHidePlayed = YES;	
 WPHidePresence = YES;	
 WPNoReadReceipts = YES;	
 
 Ignition = YES;
 
 WPHidePlayed = [preferences[PREFERENCES_ENABLED_WPHidePlayed_KEY] boolValue];
 WPHidePresence = [preferences[PREFERENCES_ENABLED_WPHidePresence_KEY] boolValue];
 WPNoReadReceipts = [preferences[PREFERENCES_ENABLED_WPNoReadReceipts_KEY] boolValue];
 
 Ignition = [preferences[PREFERENCES_ENABLED_SKPIgnition_KEY] boolValue];
	    
    //Check if 'Activate' switch is on.
	if(Ignition){ 
	NSLog(@"Curtains: curtains Activated: Initializing");

	// If ''Hide Presence' switch is activated.
	if(WPHidePresence){NSLog(@"Curtains: PresenceHooks Activated"); 
	%init(sendPresenceWithNickname); // The %init(groupName) directive simply runs whatever code exists in the targeted group. For organization
	%init(createPresenceWithType); 
	%init(internalSendPresence);
	%init(setLastPresence);
	%init(lastPresence);
	%init(updatePresence);
	%init(xmppConnection);
	%init(xmppConnectionJID);
	%init(nicknameXMPPPresenceStanza);
	%init(nicknameXMPPMessageStanza);
	lastSeen = YES;
	offLine = YES;
	showDate = YES;
	showTime = YES;
	showTimestamp = YES;
	%init(someTEST);
	} else{NSLog(@"Curtains: PresenceHooks Deactivated"); }
	
	// If  'read privately' switch is activated
	if(WPNoReadReceipts){NSLog(@"Curtains: ReadHooks Activated"); 
	%init(sendReadReceiptsIfNeeded); 
	%init(sendReadReceiptsForChatMessages); 
	} else{NSLog(@"Curtains: ReadHooks Deactivated"); }
	
	// Check if 'Listen Privately' switch is activated, too.
	if(WPHidePlayed){NSLog(@"Curtains: PlayedHooks Activated"); 
	%init(sendPlayedReceiptForMessage); 	
	%init(internalSendPlayedReceiptForMessage);
	} else{NSLog(@"Curtains: PlayedHooks Deactivated"); }
	
	}
	else{
	NSLog(@"Curtains: curtains Deactivated. Target will run unmodified.");
	}
	
	}
	CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, PreferencesChangedCallback, CFSTR(PREFERENCES_CHANGED_NOTIFICATION), NULL, CFNotificationSuspensionBehaviorCoalesce);
    }];
}
